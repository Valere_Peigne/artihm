module EvalSpec (main, spec) where

import Test.Hspec

import Eval
import Expr

main :: IO ()
main = hspec spec

spec :: Spec
spec = do
    describe "eval" $ do
        it "eval (Val 12)" $ eval (Val 12) `shouldBe` 12