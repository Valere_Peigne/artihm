-- |
-- Module: Eval
-- This is my Eval module
module Eval where

import Expr

-- | Evalue une expression et renvoie un entier
eval :: Expr -> Int
eval (Val x) = x
eval (Add e1 e2) = eval e1 + eval e2
eval (Mul e1 e2) = eval e1 * eval e2

